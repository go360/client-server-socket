package com.vimcar.server;

import org.apache.commons.cli.*;

import java.io.IOException;
import java.util.Objects;

public class Application {
    public static void main(String args[]) throws IOException {

        final Options options = new Options();

        final Option portOption = new Option("p", "port", true, "Input Server Port");
        portOption.setRequired(false);
        options.addOption(portOption);

        final CommandLineParser parser = new DefaultParser();
        HelpFormatter formatter = new HelpFormatter();
        CommandLine cmd = null;

        try {
            cmd = parser.parse(options, args);
        } catch (ParseException e) {
            System.err.println(e.getMessage());
            formatter.printHelp("Vim-Car-Server", options);
            System.exit(1);
        }

        int port = 5000;
        final String portOptionValue = cmd.getOptionValue("port");
        if (!Objects.isNull(portOptionValue) && !portOptionValue.isBlank())
            port = Integer.parseInt(portOptionValue);

        Server server = new Server(port);
    }
}
