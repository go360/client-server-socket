package com.vimcar.server;

import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {

    private static boolean running = true;

    // constructor with port
    public Server(int port) throws IOException {
        // starts server and waits for a connection

        //try(){} Resources help to close the streams automatically
        try (ServerSocket server = new ServerSocket(port)) {
            while (running) {
                try {
                    try (Socket socket = server.accept()) {
                        try (DataInputStream in = new DataInputStream(new BufferedInputStream(socket.getInputStream()))) {
                            write(in.readAllBytes());
                        }
                    }
                } catch (IOException e) {
                    System.err.println(e);
                }
            }
        }
    }

    public void write(final byte[] data) {
        new Thread(() -> {
            //Cannot user println() because it returns different Line Separators on different platforms
            // Windows: "\r\n" , Unix: "\n" -> which will cause problem in the unix system file compare.
            System.out.print(new String(data));
        }).start();
    }

    public void close() {
        running = false;
    }

}
