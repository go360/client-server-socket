#!/bin/bash

# Setup Bash
set -u # Fail on unknown variables
set -e # Fail on failed statements, don't skip over them
set -o pipefail # Also fail on failed statements in pipes

# Setup Variables
INPUT_FILE=${1:-sample_input.txt}
DIR="$(pwd)"

# Setup temporary directory
tmpdir=$DIR
echo "Temporary Output will be in $tmpdir" >&2

# Setup a trap which cleans the temporary files
# trap 'echo "Cleaning temporary files" >&2; rm -rf -- $tmpdir' EXIT

# Start Server in the background, capturing its stdin and remembering its pid
echo "Starting Server in Background" >&2
$DIR/server.sh > $tmpdir/server.out &
export SERVER_PID=$!

# Wait for server to start
sleep 1

# Check that the server is actually running and did not exit already
if ! kill -0 $SERVER_PID > /dev/null 2>&1; then
  echo "Server has not started, failure"
  exit 1
fi

# Read the input file and pass it to the client
echo "Simulating sending data from $INPUT_FILE" >&2
cat $INPUT_FILE | while read l; do
  # Sleep for a random time between 0 and 1 second
  # this simulates some latency when reading the sensor data
  sleep 0.001
  echo $l
done | $DIR/client.sh
echo "Finished simulating data from $INPUT_FILE" >&2

# Wait for some seconds for the server to finish its stuff if it is still running
if kill -0 $SERVER_PID > /dev/null 2>&1; then
  echo "Waiting for server to stop" >&2
  sleep 3
fi

# Stop the server if it is still running
if kill -0 $SERVER_PID > /dev/null 2>&1; then
  echo "Forcing server to stop"
  kill $SERVER_PID &>/dev/null || true
fi

# Sort what the server recieved and the input file, we don't care for order
sort $tmpdir/server.out > $tmpdir/server.out.sorted
sort $INPUT_FILE > $tmpdir/input.sorted

# Diff the input and the output
EXIT_CODE=0
diff $tmpdir/server.out.sorted $tmpdir/input.sorted || EXIT_CODE=$?
if [ $EXIT_CODE -eq 0 ]; then
  echo "Acceptance test: Pass"
else
  echo "Acceptance test: Fail"
  echo "------- Sent --------"
  cat $INPUT_FILE
  echo "-------- Received -------"
  cat $tmpdir/server.out
fi