# Project
Name: client-server

# Code compile and run
There are two programs in the code.
* vim-car-client
* vim-car-server

* /vim-car-client mvn package
* /vim-car-server mvn package

* /vim-car-client/target/vim-client.jar (Executable jar)
* /vim-car-server/target/vim-server.jar (Executable jar)

* Copy Jars and place in /client-server directory  where we have [acceptance_test.sh , server.sh and client.sh]

# Client 
* vim-client.jar
* Input by command line arguments -m (But Java command line argements automaticaly remove double quotes from the argument which will not equalize with the input string)
* File input -f 
* Keyboard Screen input (Default)
* Examples (vim-client.jar -m "Hello") (vim-client.jar -f sample_test.txt) 
* Availble options (-p port, -h host, -m message, -f file)

# Test Results
- Temporary Output will be in /e/Projects/IntelliJ/vim-car
- Starting Server in Background
- Simulating sending data from sample_input.txt
- Connecting ... 127.0.0.1:5000
- Finished simulating data from sample_input.txt
- Waiting for server to stop
- Forcing server to stop
- Acceptance test: Pass

# Edge case
* While comparision with the two sorted files, make sure the files having same EOL convention Windows (CR LF), Unix (LF), MacOS(CR) 
* In the given example the sample_file.txt is Unix (LF)

# Contact person
mysialkot@hotmail.com




