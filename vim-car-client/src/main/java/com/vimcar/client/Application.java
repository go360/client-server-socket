package com.vimcar.client;

import org.apache.commons.cli.*;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Objects;
import java.util.Scanner;

/**
 * @author Sikandar Ali Awan
 * @since 07-09-2020
 */

public class Application {

    static int port = 5000;
    static String host = "127.0.0.1";

    public static void main(String args[]) {

        final Options options = new Options();

        final Option portOption = new Option("p", "port", true, "Input Server Port <Optional - Default 5000>");
        portOption.setRequired(false);
        options.addOption(portOption);

        final Option hostOption = new Option("h", "host", true, "Input Server Host <Optional - Default 127.0.0.1>");
        hostOption.setRequired(false);
        options.addOption(hostOption);

        final Option messageOption = new Option("m", "message", true, "Input Message <Required>");
        messageOption.setRequired(false);
        options.addOption(messageOption);

        final Option fileOption = new Option("f", "file", true, "Input Messages from file <Optional>");
        fileOption.setRequired(false);
        options.addOption(fileOption);

        final CommandLineParser parser = new DefaultParser();
        final HelpFormatter formatter = new HelpFormatter();
        CommandLine cmd = null;

        try {
            cmd = parser.parse(options, args);
        } catch (ParseException e) {
            System.out.println(e.getMessage());
            formatter.printHelp("Vim-Car-Client", options);
            System.exit(1);
        }

        try {

            if (Objects.nonNull(cmd.getOptionValue("host")))
                host = cmd.getOptionValue("host");
            if (Objects.nonNull(cmd.getOptionValue("port")))
                port = Integer.parseInt(cmd.getOptionValue("port"));

            final Client client = new Client(host, port);

            System.out.println(String.format("Connecting ... %s:%d", host, port));

            if (Objects.nonNull(cmd.getOptionValues("file"))) {
                Files.readAllLines(Path.of(cmd.getOptionValue("file"))).forEach(line ->
                        client.send(line + "\n")
                );
            } else {
                if (Objects.nonNull(cmd.getOptionValues("message"))) {
                    client.send(cmd.getOptionValue("message") + "\n");
                } else {
                    //This will keep running until we close!
                    InputStreamReader in = new InputStreamReader(System.in);
                    int letter = 0;
                    StringBuilder sb = new StringBuilder();
                    while ((letter = in.read()) != -1) {
                        sb.append((char) letter);
                        if (letter == (int) '\n') {
                            client.send(sb.toString());
                            sb = new StringBuilder();
                        }
                    }
                    in.close();
                }
            }

        } catch (NumberFormatException | IOException e) {
            System.err.println("Invalid Port: " + args[1]);
        }

    }
}
