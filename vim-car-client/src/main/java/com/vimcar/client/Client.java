package com.vimcar.client;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Objects;

/**
 * @author Sikandar Ali Awan
 * @since 07-09-2020
 */

public class Client {

    // constructor to put ip address and port
    private String address;
    private int port;

    public Client(String address, int port) {
        this.address = address;
        this.port = port;
    }

    public void send(String message) {
        // establish a connection
        try (Socket socket = new Socket(address, port)) {
            try (DataOutputStream out = new DataOutputStream(socket.getOutputStream())) {
                try {
                    if (Objects.nonNull(message))
                        out.write(message.getBytes());
                    else {
                        System.err.print("  <Blank input Message> ");
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}