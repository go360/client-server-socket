# Backend Developer Candidate Test

This is a simple client and server for sending and receiving sensor data.

## Objectives

The purpose of completing this test is to show us how you approach and solve a problem. Ideally you should not spend more than 2-3 hours on this. Please solve the problem in a way that you think would be suitable for production.

- *State your assumptions.* Anywhere you feel that the requirements are unclear please make an assumption and document that assumption.
- *Describe Trade-offs.* When you're making a decision about using one design/approach vs. another try to make a quick note about why you made the choice you did.
- *Provide tests.* You should provide unit tests for the code that you write. The choice of testing tools is up to you.
- In addition to the above documentation and tests please also provide the source code for you solution. :)


## Requirements

This system simulates a client sending data readings to a remote server. We would like you to imagine that the client and server are separated by a network connection with high data transmission costs. Please make an effort to **minimize the number of bytes being sent** between the client and server. Messages don't need to be received in real-time, but keep latency below 2 seconds for any given message.

Please keep in mind that we are much more interested in seeing a well-designed and well-tested solution than we are in getting the absolute best data compression.

Please write a client and a server. They should run as separate processes. Their requirements are specified below.

### Client

The client will receive sensor data on `STDIN`. Each line (delimited by `\n`) will represent one message. Your job is to handle these messages and send them in an efficient manner to the server.

### Server

The server will print the received messages to `STDOUT`. If you have other things you want to output please use `STDERR`.

## Test Script

We have provided a bash script for doing acceptance testing. It will run both the client and server. The script will send simulated sensor data, one message per line, to `STDIN` on the client. These messages will be sent at random intervals (between 0 and 1000ms) to simulate real-time readings. The script captures `STDOUT` from the server, sorts it and then compares it to the input. If they are the same the test passes. This should help to evaluate your solution.

NOTE: You need to update client.sh and server.sh so that they start your client and server processes.
